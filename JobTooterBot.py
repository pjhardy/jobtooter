# Borrows heavily from ananas' TraceryBot, except I wanted
# to use aparrish's tracery library instead.
import json
import random

import ananas
import pronouncing
import tracery
from tracery.modifiers import base_english
from ananas import PineappleBot, ConfigurationError

class JobTooterBot(PineappleBot):
    def init(self):
        self.config.root_symbol = "origin"

    def start(self):
        if "grammar_file" not in self.config:
            raise ConfigurationError("JobTooterBot requires a 'grammar_file'")
        if "root_symbol" not in self.config:
            raise ConfigurationError("JobTooterBot requires a 'root_symbol'")
        if "cw_symbol" not in self.config:
            raise ConfigurationError("JobTooterBot requires a 'cw_symbol'")
        with open(self.config.grammar_file, "r") as f:
            if f:
                rules = json.load(f)
                # Finagling of the rules here
                rules["doing"] = pronouncing.search("IH0 NG$")

                self.grammar = tracery.Grammar(rules)
                self.grammar.add_modifiers(base_english)
            else: raise ConfigurationError("Couldn't open grammar file")

    @ananas.schedule(hour="*/4", minute=37)
    def post(self):
        toot_text = self.grammar.flatten("#{}#".format(self.config.root_symbol))
        toot_warning = self.grammar.flatten("#{}#".format(self.config.cw_symbol))
        self.mastodon.status_post(spoiler_text=toot_warning,
                                  visibility="public",
                                  status=toot_text)
